This is based on the [Linux instructions](https://www.vagrantup.com/downloads.html).

## Install Vagrant locally

To install Vagrant locally via `sudo`, using Ansible and Bash, clone this repo and run the install script:

```bash
git clone https://git.coop/webarch/vagrant.git
cd vagrant
./install.sh
```

## Ansible Galaxy

This repo should also be able to be used as an Ansible Galaxy role since the `roles` directory only contains symlinks.

Add the following to your `requirements.yml`:

```yml
- name: vagrant
  src: https://git.coop/webarch/vagrant.git
  version: master
  scm: git
```

And run:

```bash
ansible-galaxy install -r requirements.yml --force
```
